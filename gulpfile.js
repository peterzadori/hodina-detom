var gulp = require('gulp'),
    less = require('gulp-less'),
    pleeease = require('gulp-pleeease');

gulp.task('compile', function() {
    gulp.src(['assets/css/style.less'])
        .pipe(less())
        .pipe(pleeease())
        .pipe(gulp.dest('assets/dist/css'));
});

gulp.task('images', function() {
    gulp.src(['assets/images/*'])
        .pipe(gulp.dest('assets/dist/images'))
});

gulp.task('watch', ['compile', 'images'], function() {
    gulp.watch('assets/css/**/*.less', ['compile']);
});