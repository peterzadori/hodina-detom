(function($) {

    'use strict';

    var $donateBar = $('.donate-bar'),
        donateBarOffset = 100;

    $(document).on('scroll', function() {
        var scrollTop = $(document).scrollTop();

        if (scrollTop > donateBarOffset) {
            $donateBar.addClass('sticky');
        } else if ($donateBar.hasClass('sticky')) {
            $donateBar.removeClass('sticky');
        }
    });

})(jQuery);